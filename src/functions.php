<?php
namespace Framework\Smarter;

function json_encode($data)
{
    $json = \json_encode($data);

    if (\json_last_error() !== \JSON_ERROR_NONE) {
        throw new JsonException(\json_last_error_msg());
    }

    return $json;
}

function json_decode($payload)
{
    $data = \json_decode($payload);

    if (\json_last_error() !== \JSON_ERROR_NONE) {
        throw new JsonException(\json_last_error_msg());
    }

    return $data;
}
